from flask import Flask, request, redirect, jsonify, render_template, url_for
import torch
from torch import nn
import os
from PIL import Image
from torchvision import transforms
from werkzeug.utils import secure_filename
import numpy as np
import torch.nn.functional as F
from beheaded_inception3 import beheaded_inception_v3


app = Flask(__name__, static_folder='static', template_folder='templates')

# Configure the app
app.config['UPLOAD_FOLDER'] = 'uploads'  # Relative to where the server is run
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024  # Max upload size is 16MB

# Define the CaptionNet architecture
class CaptionNet(nn.Module):
    def __init__(self, n_tokens=10403, emb_size=64, lstm_units=128, cnn_feature_size=2048):
        super(CaptionNet, self).__init__()
        self.cnn_to_h0 = nn.Linear(cnn_feature_size, lstm_units)
        self.cnn_to_c0 = nn.Linear(cnn_feature_size, lstm_units)
        self.embedding = nn.Embedding(num_embeddings=n_tokens, embedding_dim=emb_size)
        self.lstm = nn.LSTM(input_size=emb_size, hidden_size=lstm_units, batch_first=True)
        self.logits = nn.Linear(in_features=lstm_units, out_features=n_tokens)

    def forward(self, image_vectors, captions_ix):
        self.lstm.flatten_parameters()
        initial_cell = self.cnn_to_c0(image_vectors)
        initial_hid = self.cnn_to_h0(image_vectors)
        embed = self.embedding(captions_ix)
        lstm_out, (h, c) = self.lstm(embed, (initial_cell[None], initial_hid[None]))
        logits_comp = self.logits(lstm_out)
        return logits_comp

# Initialize the image transformation
transform = transforms.Compose([
    transforms.Resize((299, 299)),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])

# Initialize the Inception model
inception = beheaded_inception_v3().eval()

# Function to load the model
def load_model(model_path):
    network = CaptionNet(n_tokens=10403, emb_size=128, lstm_units=256)
    checkpoint = torch.load(model_path, map_location='cpu')
    network.load_state_dict(checkpoint['model_state_dict'])
    network.eval()
    return network

# Load model
network = load_model('web_app_v4.pth')

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'})

    file = request.files['file']
    if file.filename == '':
        return jsonify({'error': 'No selected file'})

    if file:
        filename = secure_filename(file.filename)
        filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(filepath)
        # Return the file name, so it can be used by the client to refer to the uploaded file
        return jsonify({'message': 'File uploaded successfully', 'filename': filename})


@app.route('/predict', methods=['POST'])
def predict():
    # Expect a json with the filename
    data = request.json
    filename = data.get('filename')
    
    if not filename:
        return jsonify({'error': 'No filename provided'})

    filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)

    if not os.path.isfile(filepath):
        return jsonify({'error': 'File not found'})

    # Preprocess the image and prepare it for the model
    image = Image.open(filepath).convert('RGB')  # Convert to RGB to ensure it has 3 channels
    image = transform(image).unsqueeze(0)  # Apply the transform and add batch dimension

    # Generate a caption
    caption = generate_caption(image)  # Use the generate_caption function

    # Return the generated caption
    return jsonify({'caption': caption})

vocab_path = 'vocab.txt'
with open(vocab_path, 'r') as file:
    vocab = [line.strip() for line in file.readlines()]

word_to_index = {w: i for i, w in enumerate(vocab)}

eos_ix = word_to_index['#END#']
unk_ix = word_to_index['#UNK#']
pad_ix = word_to_index['#PAD#']


def as_matrix(sequences, max_len=None):
    # Convert a list of tokens into a matrix with padding
    max_len = max_len or max(map(len,sequences))
    
    matrix = np.zeros((len(sequences), max_len), dtype='int32') + pad_ix
    for i,seq in enumerate(sequences):
        row_ix = [word_to_index.get(word, unk_ix) for word in seq[:max_len]]
        matrix[i, :len(row_ix)] = row_ix
    
    return matrix


def generate_caption(image_tensor, caption_prefix = ('#START#',), t=1, sample=True, max_len=25):
    net = network.cpu().eval()
    

    vectors_8x8, vectors_neck, logits = inception(image_tensor)
    caption_prefix = list(caption_prefix)

    for _ in range(max_len):
        
        prefix_ix = as_matrix([caption_prefix])
        prefix_ix = torch.tensor(prefix_ix, dtype=torch.int64)
        next_word_logits = network.forward(vectors_neck, prefix_ix)[0, -1]
        next_word_probs = F.softmax(next_word_logits, -1).detach().numpy()
        
        assert len(next_word_probs.shape) == 1, 'probs must be one-dimensional'
        next_word_probs = next_word_probs ** t / np.sum(next_word_probs ** t) # apply temperature

        if sample:
            next_word = np.random.choice(vocab, p=next_word_probs) 
        else:
            next_word = vocab[np.argmax(next_word_probs)]

        caption_prefix.append(next_word)

        if next_word == '#END#':
            break

    return ' '.join(caption_prefix[1:-1])

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)
