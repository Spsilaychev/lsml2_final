var form = document.forms.namedItem("fileinfo");
var lastImage;

/*
!!!!!!!!!!!!!!!!!!
SOURCE: https://github.com/Sebastian-Schuchmann/ChurrosSamosaClassifier/tree/main
!!!!!!!!!!!!!!!!!!
*/


form.addEventListener('submit', function (ev) {
  ev.preventDefault();

  document.getElementById("loader").style.visibility = "visible";
  let formdata = new FormData(form);

  var requestOptions = {
    method: 'POST',
    body: formdata,
  };

  var loc = window.location;

  // Directly submit to /upload first
  fetch(`${loc.protocol}//${loc.hostname}:${loc.port}/upload`, requestOptions)
    .then(response => response.json())
    .then(data => {
      if (data.filename) {
        // If file upload is successful, send a request to /predict
        return fetch(`${loc.protocol}//${loc.hostname}:${loc.port}/predict`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ filename: data.filename })
        });
      }
      throw new Error('File upload did not return filename');
    })
    .then(response => response.json())
    .then(result => {
      // Handle the response from /predict
      console.log(result);
      DisplayResult(result.caption || result.error); // Use the caption from the response
      document.getElementById("loader").style.visibility = "hidden";
    })
    .catch(error => {
      console.error('Error:', error);
      document.getElementById("loader").style.visibility = "hidden";
    });
});

//On Image Uploaded
form.addEventListener('change', function (ev) {
  lastImage = URL.createObjectURL(ev.target.files[0]);
  document.getElementById("uploadedImg").src = lastImage;
})

function DisplayResult(result) {
  var resultDiv = document.getElementById("results");

  var divContainer = document.createElement("div");
  divContainer.className = "result";

  var divText = document.createElement("div");
  divText.appendChild(document.createTextNode(result));
  divText.className = "text";

  divContainer.appendChild(divText);
  resultDiv.prepend(divContainer);
}
