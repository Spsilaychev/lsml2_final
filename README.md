# Image Captioning Service

This project provides a service for generating captions for images using a deep learning model based on PyTorch. Below you'll find documentation for project setup, data sets, model training, and service deployment.

## 1. Project Documentation

### 1.1. Design Document

The design document provides an overview of the system's architecture, including the machine learning model and the web service used to serve predictions. It outlines the flow of data through the system and the interactions between the components.

The image recognition model generates captions based on the training dataset of vectorized images and tokenized captions. The key module ised is PyTorch. In uses CaptionNet class, that converts features, creates embedding for input words based on pre-defined barameters. CaptionNet class  leverages the LSTM as a recurrent core for the network. After it, is creates linear layer that thakes LTSM hidden state as input and computes one number per token.

The ipynb document contains functions to calculate necessary metrics to evaluate model correctness (e.g., losses between logits output and captions) and examples of pictures on the large model and simplified model for mobile apps. 

### 1.2. Run Instructions

To run the service locally, follow these steps:

#### Environment Setup

##### Clone the repository
```bash
git clone https://gitlab.com/Spsilaychev/lsml2_final.git
cd lsml2_final
```

please note: data for image_codes.npy is LFS, so it will take time to upload. However, for the web app itself, it is not critical to have it.

##### Create a virtual environment (optional - for me works without this step, so I recommend skipping it)
```bash
python -m venv venv
source venv/bin/activate
```

##### Install dependencies
```bash
pip install -r server/requirements.txt
```

##### To start the Flask service
```bash
cd server
python main.py
```
##### For launch, refer to step 4.3


### 1.3. Architecture, Losses, Metrics

The architecture of the neural network, the losses used during training, and the metrics used to evaluate the model's performance are detailed in the Jupyter Notebook. Brief introduction of the model techniques used were provided in 1.1.
To provide the brief on losses, two models were used: large one (n_epochs=35, emb_size=128, lstm_units=256, batch size=128) and smaller one that is used for web application (n_epochs=12, emb_size=64, lstm_units=128, batch size=128). Lossess are about 2.5% in train dataset, and 2.6% in validation dataset, both models perform approximately the same. Since the size and complexity of the model in latter case is smaller, it was used for a web app.

Please note, that on a deployment stage there is misalignment of the preprocessing stage that affect the result of the image captioning: it still generates captions, but the quality drops on this stage. In the ipynb you can see that the model works with pictures in a good manner (you can test it on your own pictures).

## 2. Data Set

The data set used for training the model can be found in the project_notebook/data directory. It contains the following:

captions_tokenized.json: Tokenized captions for the images.
image_codes.npy: Encoded images feature vectors.
size of the dataset is 118287 observations each.

## 3. Model Training Code

### 3.1. Jupyter Notebook
The model training is documented in the Jupyter Notebook project_notebook/lsml2_final_image_captioning_Silaychev.ipynb. This includes data preprocessing, model architecture, training, and evaluation.

### 3.2. MLFlow Project
MLFlow is not used in the model, since based on Anatoly's input it is not necessary to use it.

## 4. Service Deployment and Usage Instructions

### 4.1. Dockerfile or docker-compose file
The Dockerfile in the root directory defines the container image for the service. Use the following command to build and run the service:
```bash
docker build -t img-caption-service .
docker run -p 5000:5000 img-caption-service
```

### 4.2. Required Services: Databases
The current version of the service does not require an external database. However, the data used for training is located in the project_notebook/data. In addition, server/vocab.txt contains the vocabulary that is used for captions generation. In addition, there is module server/beheaded_inception3.py that is used in main.py as external module not available via pip installation procedure.

### 4.3. Client for Service
The client interface is a web page served at http://localhost:5000/ or at http://192.168.1.61:5001/ (check terminal output in case of other location) after starting the service, which allows users to upload images and receive captions.

To use it, in the browser locate the "Choose File" button and select image from your laptop. After uploading, tap the "CAPTION!" button to generate the result.

### 4.4. Model
The pre-trained model file web_app_v4.pth is used by the Flask service to generate captions and should be placed in the root of the server directory.






